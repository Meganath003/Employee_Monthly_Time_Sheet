(function($,sr){

  // debouncing function from John Hann
  // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
  var debounce = function (func, threshold, execAsap) {
      var timeout;

      return function debounced () {
          var obj = this, args = arguments;
          function delayed () {
              if (!execAsap)
                  func.apply(obj, args);
              timeout = null;
          };

          if (timeout)
              clearTimeout(timeout);
          else if (execAsap)
              func.apply(obj, args);

          timeout = setTimeout(delayed, threshold || 100);
      };
  }
  // smartresize 
  jQuery.fn[sr] = function(fn){  return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');







(function(){

	$wrapper = $('#wrapper');
	$drawerRight = $('#drawer-right');

	///////////////////////////////
	// Set Home Slideshow Height
	///////////////////////////////

	/*function setHomeBannerHeight() {
		var windowHeight = jQuery(window).height();	
		jQuery('#header').height(windowHeight);
	}*/

	///////////////////////////////
	// Center Home Slideshow Text
	///////////////////////////////

	/*function centerHomeBannerText() {
			var bannerText = jQuery('#header > .center');
			var bannerTextTop = (jQuery('#header').actual('height')/2) - (jQuery('#header > .center').actual('height')/2) - 40;		
			bannerText.css('padding-top', bannerTextTop+'px');		
			bannerText.show();
	}*/



	///////////////////////////////
	// SlideNav
	///////////////////////////////

	function setSlideNav(){
		jQuery(".toggleDrawer").click(function(e){
			//alert($wrapper.css('marginRight'));
			e.preventDefault();

			if($wrapper.css('marginLeft')=='0px'){
				$drawerRight.animate({marginRight : 0},500);
				$wrapper.animate({marginLeft : -300},500);
			}
			else{
				$drawerRight.animate({marginRight : -300},500);
				$wrapper.animate({marginLeft : 0},500);
			}
			
		})
	}

	function setHeaderBackground() {		
		var scrollTop = jQuery(window).scrollTop(); // our current vertical position from the top	
		
		if (scrollTop > 300 || jQuery(window).width() < 700) { 
			jQuery('#header .top').addClass('solid');
		} else {
			jQuery('#header .top').removeClass('solid');		
		}
	}







	setSlideNav();
	jQuery.noConflict();
	setHomeBannerHeight();
	centerHomeBannerText();
	setHeaderBackground();

	//Resize events
	jQuery(window).smartresize(function(){
		setHomeBannerHeight();
		centerHomeBannerText();
		setHeaderBackground();
	});


	//Set Down Arrow Button
	jQuery('#scrollToContent').click(function(e){
		e.preventDefault();
		jQuery.scrollTo("#services", 1000, { offset:-(jQuery('#header .top').height()), axis:'y' });
	});

	jQuery('nav > ul > li > a').click(function(e){
		e.preventDefault();
		jQuery.scrollTo(jQuery(this).attr('href'), 400, { offset:-(jQuery('#header .top').height()), axis:'y' });
	})

	jQuery(window).scroll( function() {
	   setHeaderBackground();
	});

})();

//Navigation for admin page////

function openNavxx() {
    document.getElementById("drawer-right2").style.width = "70%";
}

function openNavx() {
    document.getElementById("mySidenavx").style.width = "78%";
	document.getElementById("main").style.marginLeft = "81%";
}
function openNav2() {
    document.getElementById("mySidenav2").style.width = "78%";
	document.getElementById("mySidenav3").style.width = "0";
	document.getElementById("mySidenav4").style.width = "0";
	document.getElementById("mySidenav5").style.width = "0";
	document.getElementById("mySidenav6").style.width = "0";
	document.getElementById("main").style.marginLeft = "81%";
}
function openNav3() {
    document.getElementById("mySidenav3").style.width = "78%";
	document.getElementById("mySidenav2").style.width = "0";
	document.getElementById("mySidenav4").style.width = "0";
	document.getElementById("mySidenav5").style.width = "0";
	document.getElementById("mySidenav6").style.width = "0";
	document.getElementById("main").style.marginLeft = "81%";
}
function openNav4() {
    document.getElementById("mySidenav4").style.width = "78%";
	document.getElementById("mySidenav2").style.width = "0";
	document.getElementById("mySidenav3").style.width = "0";
	document.getElementById("mySidenav5").style.width = "0";
	document.getElementById("mySidenav6").style.width = "0";
	document.getElementById("main").style.marginLeft = "81%";
}
function openNav5() {
    document.getElementById("mySidenav5").style.width = "78%";
	document.getElementById("mySidenav2").style.width = "0";
	document.getElementById("mySidenav3").style.width = "0";
	document.getElementById("mySidenav4").style.width = "0";
	document.getElementById("mySidenav6").style.width = "0";
	document.getElementById("main").style.marginLeft = "81%";
}
function openNav6() {
    document.getElementById("mySidenav6").style.width = "78%";
	document.getElementById("mySidenav2").style.width = "0";
	document.getElementById("mySidenav3").style.width = "0";
	document.getElementById("mySidenav4").style.width = "0";
	document.getElementById("mySidenav5").style.width = "0";
	document.getElementById("main").style.marginLeft = "81%";
}

function closeNavx() {
    document.getElementById("mySidenavx").style.width = "0";
    document.getElementById("main").style.marginLeft= "20%";
}
function closeNavxx() {
    document.getElementById("drawer-right2").style.width = "0";

}
function closeNav2() {
    document.getElementById("mySidenav2").style.width = "0";
    document.getElementById("main").style.marginLeft= "20%";
}
function closeNav3() {
    document.getElementById("mySidenav3").style.width = "0";
    document.getElementById("main").style.marginLeft= "20%";
}
function closeNav4() {
    document.getElementById("mySidenav4").style.width = "0";
    document.getElementById("main").style.marginLeft= "20%";
}
function closeNav5() {
    document.getElementById("mySidenav5").style.width = "0";
    document.getElementById("main").style.marginLeft= "20%";
}
function closeNav6() {
    document.getElementById("mySidenav6").style.width = "0";
    document.getElementById("main").style.marginLeft= "20%";
}

 function closeWindow() {window.close(); window.open("Home_Login.html")}
 